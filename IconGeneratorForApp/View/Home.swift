//
//  Home.swift
//  IconGeneratorForApp
//
//  Created by ULADZISLAU SHURYN on 01.04.22.
//

import SwiftUI

struct Home: View {
    
    @StateObject var iconViewModel: IconViewModel = IconViewModel()
    
    var body: some View {
        VStack {
            if let image = iconViewModel.selectedImage {
                afterChoosingImageView(image: image)
            } else {
                beforeChoosingImageView()
            }
        }
        .frame(width: 400, height: 400)
        .buttonStyle(.plain)
        .alert(iconViewModel.alertMessage, isPresented: $iconViewModel.showAlert) {
            
        }
        .overlay {
            ZStack {
                if iconViewModel.isGenerated {
                    Color.black.opacity(0.5)
                    
                    ProgressView()
                        .padding()
                        .background(.white, in: RoundedRectangle(cornerRadius: 10))
                        .environment(\.colorScheme, .light)
                }
            }
        }
        .animation(.easeInOut, value: iconViewModel.isGenerated)
    }
    
    @ViewBuilder
    func beforeChoosingImageView() -> some View {
        ZStack {
            Button {
                iconViewModel.chooseImage()
            } label: {
                Image(systemName: "plus")
                    .font(.system(size: 22, weight: .bold))
                    .foregroundColor(Color("ButtonColor"))
                    .padding(15)
                    .background(.primary, in: RoundedRectangle(cornerRadius: 10))
            }
            
            Text("1024px x 1024px recomended")
                .font(.caption)
                .foregroundColor(.gray)
                .padding(.bottom, 20)
                .frame(maxHeight: .infinity, alignment: .bottom)
        }
    }
    
    @ViewBuilder
    func afterChoosingImageView(image: NSImage) -> some View {
        Image(nsImage: image)
            .resizable()
            .aspectRatio(contentMode: .fill)
            .frame(width: 250, height: 250)
            .cornerRadius(15)
            .clipped()
            .onTapGesture {
                iconViewModel.chooseImage()
            }
        
        Button {
            iconViewModel.generateIcons()
        } label: {
            Text("Generate icons")
                .foregroundColor(Color("ButtonColor"))
                .padding(.vertical, 10)
                .padding(.horizontal, 30)
                .background(.primary, in: RoundedRectangle(cornerRadius: 10))
        }
        .padding(.top, 20)
    }
    
}

struct Home_Previews: PreviewProvider {
    static var previews: some View {
        Home()
    }
}
