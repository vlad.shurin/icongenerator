//
//  IconGeneratorForAppApp.swift
//  IconGeneratorForApp
//
//  Created by ULADZISLAU SHURYN on 01.04.22.
//

import SwiftUI

@main
struct IconGeneratorForAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
