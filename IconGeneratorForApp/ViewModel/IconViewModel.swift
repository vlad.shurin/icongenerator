//
//  IconViewModel.swift
//  IconGeneratorForApp
//
//  Created by ULADZISLAU SHURYN on 01.04.22.
//

import SwiftUI

class IconViewModel: ObservableObject {
    
    @Published var selectedImage: NSImage?
    @Published var isGenerated: Bool = false
    @Published var showAlert: Bool = false
    @Published var alertMessage: String = ""

    @Published var iconSizes: [Int] = [
        20, 60, 58, 87, 80, 120, 180, 40, 29, 76, 152, 167,
        1024, 16, 32, 64, 128, 256, 512, 1024
    ]
    
    public func generateIcons() {
        isGenerated = true
        
        chooseFolder { [weak self] urlFolder in
            let modifiedUrl = urlFolder.appendingPathComponent("AppIcon.appiconset")
            
            DispatchQueue.global(qos: .userInteractive).async {
                do {
                    let manager = FileManager.default
                    try manager.createDirectory(at: modifiedUrl, withIntermediateDirectories: true,attributes: [:])
                    
                    if let bundleContents = Bundle.main.path(forResource: "Contents", ofType: "json") {
                        let url = URL(fileURLWithPath: bundleContents)
                        
                        try Data(contentsOf: url).write(to: modifiedUrl.appendingPathComponent("Contents.json"), options: .atomic)
                    }
                    
                    if let selectedImage = self?.selectedImage {
                        self?.iconSizes.forEach({ size in
                            let imageSize = CGSize(width: CGFloat(size), height: CGFloat(size))
                            let imageUrl = modifiedUrl.appendingPathComponent("\(size).png")
                            selectedImage.resizeImage(size: imageSize)
                                .writeImage(to: imageUrl)
                        })
                        
                        DispatchQueue.main.async {
                            self?.isGenerated = false
                            self?.alertMessage = "Success"
                            self?.showAlert.toggle()
                        }
                    }
                    
                } catch {
                    print(error.localizedDescription)
                    
                    DispatchQueue.main.async {
                        self?.isGenerated = false
                        self?.alertMessage = "Error"
                        self?.showAlert.toggle()
                    }
                }
                
                
            }
        }
    }
    
    public func chooseImage() {
        let panel = NSOpenPanel()
        panel.title = "Choose a picture"
        panel.showsResizeIndicator = true
        panel.showsHiddenFiles = false
        panel.allowsMultipleSelection = false
        panel.canChooseDirectories = false
        panel.allowedContentTypes = [.image, .png, .jpeg, .webP]
        
        if panel.runModal() == .OK {
            if let result = panel.url?.path {
                let image = NSImage(contentsOf: URL(fileURLWithPath: result))
                self.selectedImage = image
            }
        }
    }
    
    private func chooseFolder(comletion: @escaping (URL) -> ()) {
        let panel = NSOpenPanel()
        panel.title = "Choose a folder"
        panel.showsResizeIndicator = true
        panel.showsHiddenFiles = false
        panel.allowsMultipleSelection = false
        panel.canChooseDirectories = true
        panel.canChooseFiles = false
        panel.canCreateDirectories = true
        panel.allowedContentTypes = [.folder ]
        
        if panel.runModal() == .OK {
            if let result = panel.url?.path {
                comletion(URL(fileURLWithPath: result))
            }
        }
    }
}


extension NSImage {
    func resizeImage(size: CGSize) -> NSImage {
        let scale = NSScreen.main?.backingScaleFactor ?? 1
        
        let newSize = CGSize(width: size.width / scale, height: size.height / scale)
        
        let newImage = NSImage(size: newSize)
        newImage.lockFocus()
        self.draw(in: NSRect(origin: .zero, size: newSize))
        newImage.unlockFocus()
        return newImage
    }
    
    func writeImage(to: URL) {
        guard let data = tiffRepresentation,
        let representation = NSBitmapImageRep(data: data),
        let pngData = representation.representation(using: .png, properties: [:])
        else { return }
        
        try? pngData.write(to: to, options: .atomic)
    }
}
